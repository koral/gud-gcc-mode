;;; gcc-mode.el --- Gud derived mode for gcc debugging -*- lexical-binding: t; -*-

;; Maintainer: andrea_corallo@yahoo.it
;; Package: gud-gcc-mode
;; Homepage: https://gitlab.com/koral/gud-gcc-mode
;; Version: 0.1
;; Package-Requires: ((emacs "24"))
;; Keywords: unix, tools

;; This file is not part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;

;;; Code:

(require 'gud)

(defconst gcc-regex (rx
		     (seq
		      (group-n 1 (zero-or-one (one-or-more (not space)) space))
		      "<"
		      (group-n 2 (one-or-more (not space)))
		      space
		      (group-n 3 "0x" (one-or-more hex)))))

(defconst gcc-highlights
  `((,gcc-regex
     (1 font-lock-type-face)
     (2 font-lock-function-name-face)
     (3 font-lock-preprocessor-face))))

(defconst gud-gcc-mode-syntax-table
  (let ((syn-table (make-syntax-table gud-mode-syntax-table)))
    (modify-syntax-entry ?< "(>" syn-table)
    (modify-syntax-entry ?> ")<" syn-table)
    syn-table))

(defun gud-gcc-mode-init ()
  (interactive)
  (toggle-truncate-lines 1)
  (whitespace-mode -1)
  (setq font-lock-defaults '(gcc-highlights)))

(define-derived-mode gud-gcc-mode gud-mode "gcc"
  "major mode for debugging gcc."
  (gud-gcc-mode-init))

(gud-def gud-gcc-debug-print  "call debug(%e)"  "d"
	 "Print debug the expression at point.")
(gud-def gud-gcc-debug-tree-print  "call debug_tree(%e)"  "t"
	 "Print debug the tree expression at point.")
(gud-def gud-gcc-*debug-tree-print  "call debug_tree(* %e)"  "\C-g C-t"
	 "Dereference and print the tree expression at point.")

(provide 'gud-gcc-mode)

;;; gud-gcc-mode.el ends here
